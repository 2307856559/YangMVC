package org.docshare.mvc.except;

public class MVCException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4515480661510346678L;

	public MVCException(String msg) {
		super(msg);
	}

}
